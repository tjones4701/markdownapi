var functions = {};

functions.paginate = (req, query) => {
    var limit = req.getParam("limit");
    var pageSize = req.getParam("page_size", 10);
    var page = req.getParam("page", null);
    var offset = req.getParam("offset");
    
    if (limit == null && page != null) {
        limit = pageSize;
        if (offset == null) {
            offset = pageSize * (page);
        }
    }    

    if (limit != null || offset != null) {
        if (query == null) {
            query = {};
        }
    }
    
    if (limit != null){
        query.limit = parseInt(limit);
    }
    if (offset != null) {
        query.offset = parseInt(offset);
    }

    return query;
}

module.exports = functions;