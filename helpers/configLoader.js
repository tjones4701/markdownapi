var load = function (dir) {
    var env = process.env.NODE_ENV || 'dev';
    var config = require(dir + "/config/app.json");
    var envConfig = require(dir + "/config/env/app_" + env + ".json");

    return envConfig;
};

module.exports = load;