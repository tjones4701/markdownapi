
const urlHelper = require('url');

const defaultErrors = {
    400: { status: 400, error: "MISSING_PARAMETERS", description: "Missing Required Parameters" },
    401: { status: 401, error: "NOT_AUTHORIZED", description: "Not Authorized" },
    404: { status: 404, error: "NO_RESULTS", description: " No Results Found" },
    500: { status: 500, error: "ERROR", description: "Internal Error Occurred" }
};

buildRoutes = (app, apis, router) => {

    console.log("---------------------------");
    console.log("Loading Routes");
    console.log("---------------------------");

    var requestTypes = [
        "get",
        "post",
        "patch",
        "put",
        "delete"
    ];


    addToDefinitions = (api, requestType) => {
        var version = api.version;
        version = "/v" + version;
        var path = api.path;

        if (apiDefinitions[version] == null) {
            apiDefinitions[version] = {};
        }
        if (apiDefinitions[version][path] == null) {
            apiDefinitions[version][path] = [];
        }

        var fields = [
            "path",
            "public",
            "description"
        ];

        var data = {};
        for (var i in fields) {
            var field = fields[i];
            if (api[field] != null) {
                data[field] = api[field];
            }
        }

        data.methods = [];
        for (var i in requestTypes) {
            var type = requestTypes[i];
            if (api[type] != null) {
                data.methods.push(type);
            }
        }
        apiDefinitions[version][path] = data;

    }

    var apiDefinitions = {}

    for (var apiPath in apis) {
        var api = apis[apiPath];

        var addedRequestTypes = [];

        for (var requestKey in requestTypes) {
            var requestType = requestTypes[requestKey];
            if (api.base != null || api[requestType] != null) {
                addedRequestTypes.push(requestType);
            }
            addToDefinitions(api, requestType);

            router[requestType](apiPath, (req, res) => {
                try {
                    var method = req.method.toLowerCase();
                    var route = req.route;

                    var urlParts = urlHelper.parse(req.url, true);
                    var pathname = urlParts.pathname;

                    var path = route.path;
                    path = path.substring(0, path.length);

                    var url = pathname.substring(path.length + 1, pathname.length);

                    req.urlParams = [];
                    var split = url.split("/");
                    for (var i in split) {
                        if (split[i].length > 0) {
                            req.urlParams.push(split[i]);
                        }
                    }

                    req.getParam = function (id, def) {
                        if (req.body != null && req.body[id] != null) {
                            return req.body[id];
                        } else if (req.query != null && req.query[id] != null) {
                            return urlParts.query[id];
                        } else if (req.params != null && req.params[id] != null) {
                            return req.params[id];
                        } else {
                            if (def == null) {
                                def = null;
                            }
                            return def;
                        }
                    }


                    req.getParams = (params) => {
                        var item = {};
                        for (var paramKey in params) {
                            var param = params[paramKey];
                            var val = req.getParam(param, undefined);
                            if (val != undefined) {
                                item[param] = val;
                            }
                        }
                        return item;
                    }

                    req.requiredParameters = (req, params) => {
                        var incorrectParameters = [];
                        for (var paramKey in params) {
                            var param = params[paramKey];
                            if (req.getParam(param) == null) {
                                incorrectParameters.push(param);
                            }
                        }
                        if (incorrectParameters.length > 0) {
                            return new Promise((resolve, reject) => {
                                reject(400, "MISSING_PARAMETERS", "The following are missing: " + incorrectParameters.join(", "));
                            });
                        }
                        return null;
                    }

                    if (apis[path] != null) {

                        var methodFunction = apis[path][method];
                        if (methodFunction == null && apis[path].base != null) {
                            methodFunction = apis[path].base;
                        }
                        if (methodFunction != null) {

                            var promise = methodFunction(app, req, res);

                            if (promise != null && promise.then != null) {
                                promise.then((data, code) => {
                                    if (code == null) {
                                        code = 200;
                                    }
                                    res.status(code);
                                    var results = 1;
                                    if (data instanceof Array) {
                                        results = data.length;
                                    }
                                    res.json({ results: results, data: data });
                                }).catch((errorCode, error, description) => {
                                    var code = parseInt(errorCode);

                                    if (isNaN(errorCode)) {
                                        console.log(errorCode);
                                        res.status(500);
                                        res.send({ error: { status: 500, error: "Internal Error Occured", description: errorCode } });
                                        return;
                                    }
                                    if (code == null || code == "NaN") {
                                        code = "500";
                                    }

                                    if (error == null) {
                                        if (defaultErrors[code] != null && defaultErrors[code].error != null) {
                                            error = defaultErrors[code].error;
                                        } else {
                                            error = "ERROR";
                                        }
                                    }

                                    if (description == null) {
                                        if (defaultErrors[code] && defaultErrors[code].description != null) {
                                            description = defaultErrors[code].description;
                                        } else {
                                            description = "Internal Error Occurred";
                                        }
                                    }
                                    res.status(code);
                                    res.send({ error: { status: code, error: error, description: description } });
                                });
                            }
                        } else {
                            res.status(405).send({ error: { status: 405, error: "INVALID_METHOD", description: method + " not accepted at " + path } });
                        }
                    } else {
                        res.status(500).send({ error: { status: 500, error: "NO_API", description: "No api found for " + path } });
                    }
                } catch (e) {
                    console.log(e);
                }

            });

        }

        console.log(apiPath + " [" + addedRequestTypes.join(",") + "]");
    }

    //Route for getting all the api definitions
    router["get"]("/", (req, res) => {
        res.json(apiDefinitions);
    });

    //Route for getting the api definitions of a specific version
    for (var version in apiDefinitions) {
        router["get"](version, (req, res) => {
            var data = {};
            data[req.path] = apiDefinitions[req.path.replace(/\/$/, "")];
            res.json(data);
        })
    }


    console.log("-----Routes Loaded-----");

    return router;

}

module.exports = buildRoutes;