
module.exports = (sequelize, DataTypes) => {
    const organisation = sequelize.define('organisation', {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING(512),
        },
        active: {
            type: DataTypes.BOOLEAN
        }
        
    }, {
        underscored: true
    });
  
    organisation.associate = (models) => {
        
        organisation.belongsToMany(models.user, {
            foreignKey: 'organisation_id',
            primaryKey: "user_id",
            through: models.organisationUser
        });

        organisation.hasMany(models.client);
    };

    /**
     * Below are all the 
     */

    organisation.prototype.getAccess = (org, user) => {
        var promise = new Promise((resolve, reject) => {
            var id = user.dataValues.id;
            
            org.getUsers({where:{id: id}}).then((items) => {
                if (items.length == 0) {
                    reject();
                } else {
                    resolve(items);
                }
            }).catch((item) => {
                reject();
            })
        });

        return promise;
    };
    
  
    return organisation;
  };