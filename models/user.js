module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        provider: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        provider_id: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        active: {
            type: DataTypes.BOOLEAN
        }

    }, {
            underscored: true
        });

    user.associate = (models) => {
        user.belongsToMany(models.organisation, {
            foreignKey: 'user_id',
            primaryKey: "organisation_id",
            through: models.organisationUser
        });
    };


    user.getUserByProvider = (app, provider, provider_id) => {
        var query = {};
        query.where = {
            provider_id: provider_id,
            provider: provider
        }
        var promise = new Promise((resolve, reject) => {
            app.db.user.findAll(query).then((data) => {
                var user = null;
                if (data != null && data[0] != null) {
                    user = data[0];
                }
                resolve(user);
            }).catch((error) => {
                reject(error);
            });
        });
        return promise;
    }

    user.signIn = (app, provider, profile) => {
        var promise = new Promise((resolve, reject) => {
            app.db.user.getUserByProvider(app, provider, profile.id).then((user) => {
                if (user == null) {
                    var data = {
                        name: profile.displayName,
                        provider: provider,
                        provider_id: profile.id
                    }
                    app.db.user.create(data).then((instance) => {
                        resolve(instance);
                    }).catch((error) => {
                        reject(error);
                    });

                } else {
                    resolve(user);
                }
                resolve(user);
            }).catch((error) => {
                console.log(error);

                reject(error);
            })
        });
        return promise;
    }

    user.userProviderExists = (app, provider, provider_id) => {
        var self = this;
        return new Promise((resolve, reject) => {
            self.getUserByProvider(app, provider, provider_id).then((user) => {
                if (user == null) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }

    return user;
};