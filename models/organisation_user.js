module.exports = (sequelize, DataTypes) => {
    const organisationUser = sequelize.define('organisationUser', {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },
        organisation_id: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        user_id: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        user_role: {
            type: DataTypes.TEXT,
        }        
    }, {
        underscored: true,
        tableName: 'organisation_users'
    });
   
  
    return organisationUser;
  };