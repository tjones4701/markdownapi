
module.exports = (sequelize, DataTypes) => {
    const discountRecord = sequelize.define('discount_record', {
        id: {
            primaryKey: true,
            type: DataTypes.BIGINT,
            defaultValue: DataTypes.BIGINT
        },
        trading_name: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        outlet_name: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        website: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        suburb: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        postcode: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        telephone_number: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        is_online_mail_order: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        is_australia_wide: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        is_overseas: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        is_senior: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        is_carer: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        category: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        discount_description: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        lat: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        lng: {
            type: DataTypes.TEXT,
            allowNull: false,
        },


    }, {
            underscored: true
        });

    return discountRecord;
};