module.exports = (sequelize, DataTypes) => {
    const client = sequelize.define('client', {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        client_number : {
            type: DataTypes.TEXT
        },
        organisation_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        date_of_birth: {
            type: DataTypes.DATE,
        },
        active: {
            type: DataTypes.BOOLEAN
        }
    }, {
            underscored: true
        }
    );

    client.associate = (models) => {
        client.belongsTo(models.organisation);
    };

    return client;
};