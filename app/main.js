(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _services_discount_records_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/discount_records.service */ "./src/app/services/discount_records.service.ts");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/esm5/angular-bootstrap-md.es5.js");
/* harmony import */ var _components_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/side-bar/side-bar.component */ "./src/app/components/side-bar/side-bar.component.ts");
/* harmony import */ var _components_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/nav-bar/nav-bar.component */ "./src/app/components/nav-bar/nav-bar.component.ts");
/* harmony import */ var _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/welcome/welcome.component */ "./src/app/components/welcome/welcome.component.ts");
/* harmony import */ var _components_loading_loading_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/loading/loading.component */ "./src/app/components/loading/loading.component.ts");
/* harmony import */ var _components_alert_alert_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/alert/alert.component */ "./src/app/components/alert/alert.component.ts");
/* harmony import */ var _services_configuration_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/configuration.service */ "./src/app/services/configuration.service.ts");
/* harmony import */ var _components_category_category_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/category/category.component */ "./src/app/components/category/category.component.ts");
/* harmony import */ var _components_discount_record_discount_record_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/discount-record/discount-record.component */ "./src/app/components/discount-record/discount-record.component.ts");
/* harmony import */ var _components_discount_records_discount_records_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/discount-records/discount-records.component */ "./src/app/components/discount-records/discount-records.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var appRoutes = [
    {
        path: '',
        component: _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_9__["WelcomeComponent"],
        children: [
            { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"] },
            { path: 'category/:category', component: _components_category_category_component__WEBPACK_IMPORTED_MODULE_13__["CategoryComponent"] }
        ]
    },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_side_bar_side_bar_component__WEBPACK_IMPORTED_MODULE_7__["SideBarComponent"],
                _components_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_8__["NavBarComponent"],
                _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_9__["WelcomeComponent"],
                _components_loading_loading_component__WEBPACK_IMPORTED_MODULE_10__["LoadingComponent"],
                _components_alert_alert_component__WEBPACK_IMPORTED_MODULE_11__["AlertComponent"],
                _components_category_category_component__WEBPACK_IMPORTED_MODULE_13__["CategoryComponent"],
                _components_discount_record_discount_record_component__WEBPACK_IMPORTED_MODULE_14__["DiscountRecordComponent"],
                _components_discount_records_discount_records_component__WEBPACK_IMPORTED_MODULE_15__["DiscountRecordsComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_6__["MDBBootstrapModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes)
            ],
            providers: [
                _services_discount_records_service__WEBPACK_IMPORTED_MODULE_5__["DiscountRecordsService"],
                _services_configuration_service__WEBPACK_IMPORTED_MODULE_12__["ConfigurationService"]
            ],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/alert/alert.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/alert/alert.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [class]=\"class\">\r\n    <div *ngIf=\"title != null && title.length > 0\">\r\n        <b>\r\n            <i *ngIf='icon != null && icon.length > 0' [class]='\"pr-1 fa \" + icon'></i>{{title}}\r\n        </b>\r\n        <br/>\r\n    </div>\r\n    <ng-content></ng-content>\r\n</div>"

/***/ }),

/***/ "./src/app/components/alert/alert.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/alert/alert.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".persona-alert.bg-danger {\n  color: rgba(255, 255, 255, 0.9); }\n\n.persona-alert.bg-info {\n  color: rgba(0, 0, 0, 0.8); }\n\n.persona-alert.bg-warning {\n  color: rgba(0, 0, 0, 0.8); }\n"

/***/ }),

/***/ "./src/app/components/alert/alert.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/alert/alert.component.ts ***!
  \*****************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AlertComponent = /** @class */ (function () {
    function AlertComponent() {
        this.class = '';
        this.icon = '';
    }
    AlertComponent.prototype.ngOnInit = function () {
        switch (this.type) {
            case 'danger':
            case 'error':
                this.class = 'bg-danger';
                this.icon = 'fa-times-circle-o';
                break;
            case 'success':
                this.icon = 'fa-check';
                this.class = 'bg-success';
                break;
            case 'warning':
                this.icon = 'fa-warning';
                this.class = 'bg-warning';
                break;
            default:
                this.icon = 'fa-question-circle-o';
                this.class = 'bg-info';
        }
        this.class = this.class + ' card z-depth-2 w-100 h-100 p-2 persona-alert';
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AlertComponent.prototype, "type", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AlertComponent.prototype, "title", void 0);
    AlertComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/components/alert/alert.component.html"),
            styles: [__webpack_require__(/*! ./alert.component.scss */ "./src/app/components/alert/alert.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/components/category/category.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/category/category.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-discount-records [category]=\"category\"></app-discount-records>"

/***/ }),

/***/ "./src/app/components/category/category.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/category/category.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/category/category.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/category/category.component.ts ***!
  \***********************************************************/
/*! exports provided: CategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryComponent", function() { return CategoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryComponent = /** @class */ (function () {
    function CategoryComponent(route, router) {
        this.route = route;
        this.router = router;
    }
    CategoryComponent.prototype.ngOnInit = function () {
        var self = this;
        if (this.category == null) {
            this.router.events.subscribe(function (event) {
                if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                    self.category = self.route.snapshot.paramMap.get('category');
                }
            });
        }
        ;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CategoryComponent.prototype, "category", void 0);
    CategoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category',
            template: __webpack_require__(/*! ./category.component.html */ "./src/app/components/category/category.component.html"),
            styles: [__webpack_require__(/*! ./category.component.scss */ "./src/app/components/category/category.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], CategoryComponent);
    return CategoryComponent;
}());



/***/ }),

/***/ "./src/app/components/discount-record/discount-record.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/discount-record/discount-record.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='card z-depth-1 animated fadeIn h-100 p-2'>\n    <div class='row'>\n        <div class='col-8'>\n            <b>{{discountRecord.trading_name}}</b>\n            <br/>\n            <i>\n                {{discountRecord.address}}\n            </i>\n            <br/>\n            <i>\n                Distance: {{(discountRecord.distance).toFixed(2)}}kms ({{((discountRecord.distance*10)).toFixed(0)}} minute walk)\n            </i>\n            <hr/>\n\n        </div>\n        <div class='col-4 text-center'>\n            <a [href]=\"['https://www.google.com/maps/search/' + discountRecord.trading_name + '/@' + discountRecord.lat + ',' + discountRecord.lng]\"\n                class='float-right pr-2'>\n                <i class='fa fa-map-marker'></i>\n                <br/>Navigate\n            </a>\n        </div>\n        <div class='col-12'>\n            <p>\n                {{(discountRecord.discount_description)}}\n            </p>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/components/discount-record/discount-record.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/discount-record/discount-record.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/discount-record/discount-record.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/discount-record/discount-record.component.ts ***!
  \*************************************************************************/
/*! exports provided: DiscountRecordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiscountRecordComponent", function() { return DiscountRecordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DiscountRecordComponent = /** @class */ (function () {
    function DiscountRecordComponent() {
    }
    DiscountRecordComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DiscountRecordComponent.prototype, "discountRecord", void 0);
    DiscountRecordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-discount-record',
            template: __webpack_require__(/*! ./discount-record.component.html */ "./src/app/components/discount-record/discount-record.component.html"),
            styles: [__webpack_require__(/*! ./discount-record.component.scss */ "./src/app/components/discount-record/discount-record.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DiscountRecordComponent);
    return DiscountRecordComponent;
}());



/***/ }),

/***/ "./src/app/components/discount-records/discount-records.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/discount-records/discount-records.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='pr-4 pl-4 pb-4 pt-2'>\n    <ng-container *ngIf=\"!loaded; else elseTemplate\">\n        <div class='text-center'>\n            Loading\n            <app-loading></app-loading>\n        </div>\n    </ng-container>\n    <ng-template #elseTemplate>\n\n        <ng-container *ngIf=\"discountRecords == null || discountRecords.length == 0; else elseTemplate\">\n            <div class='col-12'>\n                <app-alert [type]=\"'warning'\" [title]=\"'No nearby locations'\">\n                    Oops, unfortunately we can't find any discounts within 50 km of you.\n                </app-alert>\n            </div>\n        </ng-container>\n        <ng-template #elseTemplate>\n\n\n            <app-alert [type]=\"'default'\" [title]=\"'Nearby Locations'\">\n                Below are all locations offering discounts for either senior or companion card holders.\n            </app-alert>\n            <br/>\n            <div class='row'>\n                <app-discount-record [discountRecord]=\"record\" *ngFor=\"let record of discountRecords\" class='col-12 col-lg-6 p-2'>\n                </app-discount-record>\n            </div>\n        </ng-template>\n    </ng-template>\n</div>"

/***/ }),

/***/ "./src/app/components/discount-records/discount-records.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/discount-records/discount-records.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/discount-records/discount-records.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/discount-records/discount-records.component.ts ***!
  \***************************************************************************/
/*! exports provided: DiscountRecordsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiscountRecordsComponent", function() { return DiscountRecordsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_discount_records_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/discount_records.service */ "./src/app/services/discount_records.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DiscountRecordsComponent = /** @class */ (function () {
    function DiscountRecordsComponent(_discountRecordsOService, router, route) {
        this._discountRecordsOService = _discountRecordsOService;
        this.router = router;
        this.route = route;
        this.loading = false;
        this.discountRecords = [];
        this.loaded = false;
    }
    DiscountRecordsComponent.prototype.ngOnChanges = function (changes) {
        this.loadRecords();
    };
    DiscountRecordsComponent.prototype.loadRecords = function () {
        var _this = this;
        var self = this;
        this.loaded = false;
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            var conditions = {
                limit: 50,
                'max-distance': 50
            };
            if (_this.category == null) {
                _this.category = self.route.snapshot.paramMap.get('category');
            }
            if (_this.category != null) {
                conditions['category'] = _this.category.toLowerCase();
            }
            var state = _this._discountRecordsOService.getDiscountRecords(lat, lng, conditions).subscribe(function (data) {
                if (data != null && data.data != null) {
                    _this.discountRecords = data.data;
                    _this.loaded = true;
                }
                else {
                    _this.router.navigate(['error/organisation-view/unable to load organisation']);
                }
            }, function (error) {
                console.log(error);
            });
        });
    };
    DiscountRecordsComponent.prototype.ngOnInit = function () {
        this.loadRecords();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], DiscountRecordsComponent.prototype, "category", void 0);
    DiscountRecordsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-discount-records',
            template: __webpack_require__(/*! ./discount-records.component.html */ "./src/app/components/discount-records/discount-records.component.html"),
            styles: [__webpack_require__(/*! ./discount-records.component.scss */ "./src/app/components/discount-records/discount-records.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_discount_records_service__WEBPACK_IMPORTED_MODULE_2__["DiscountRecordsService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], DiscountRecordsComponent);
    return DiscountRecordsComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='home-content'>\n    <div class='p-4'>\n        <h3>\n            Welcome to Markdown, We allow senior and companion card holders to easily discover what discounts Australian business are\n            offering them.\n        </h3>\n\n    </div>\n    <div class='row'>\n        <div class='col-12 col-lg-6 p-2'>\n            <div class='card z-depth-1 animated fadeIn text-center h-100 p-2 hoverable' [routerLink]=\"'/category/cafes'\">\n                <b>Cafes</b>\n                <br/>\n                <h2><i class='fa fa-coffee'></i></h2>\n            </div>\n        </div>\n        <div class='col-12 col-lg-6 p-2'>\n            <div class='card z-depth-1 animated fadeIn text-center h-100 p-2 hoverable' [routerLink]=\"'/category/accommodation'\">\n                <b>Accommodations</b>\n                <br/>\n                <h2><i class='fa fa-home'></i></h2>\n            </div>\n        </div>\n        <div class='col-12 col-lg-6 p-2'>\n            <div class='card z-depth-1 animated fadeIn text-center h-100 p-2 hoverable' [routerLink]=\"'/category/clothing'\">\n                <b>Clothing</b>\n                <br/>\n                <h2><i class='fa fa-user'></i></h2>\n            </div>\n        </div>\n        <div class='col-12 col-lg-6 p-2'>\n            <div class='card z-depth-1 animated fadeIn text-center h-100 p-2 hoverable' [routerLink]=\"'/category/Travel%20Agencies'\">\n                <b>Travel</b>\n                <br/>\n                <h2><i class='fa fa-car'></i></h2>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".home-content {\n  margin: 0 auto;\n  padding: 16px;\n  max-width: 800px; }\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/loading/loading.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/loading/loading.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<i class='fa fa-spinner fa-pulse'></i>"

/***/ }),

/***/ "./src/app/components/loading/loading.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/loading/loading.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/loading/loading.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/loading/loading.component.ts ***!
  \*********************************************************/
/*! exports provided: LoadingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingComponent", function() { return LoadingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingComponent = /** @class */ (function () {
    function LoadingComponent() {
    }
    LoadingComponent.prototype.ngOnInit = function () {
    };
    LoadingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-loading',
            template: __webpack_require__(/*! ./loading.component.html */ "./src/app/components/loading/loading.component.html"),
            styles: [__webpack_require__(/*! ./loading.component.scss */ "./src/app/components/loading/loading.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LoadingComponent);
    return LoadingComponent;
}());



/***/ }),

/***/ "./src/app/components/nav-bar/nav-bar.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/nav-bar/nav-bar.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex w-100 m-2 nav-title\">\r\n    <div class='navbar-title mr-4' *ngIf=\"this.type == 'base'\">\r\n        <i class='fa fa-address-card mr-2'></i>Markdown\r\n    </div>\r\n    <div class=\"flex-grow-1 nav-title\">\r\n        <small>{{title}}</small>\r\n        <div class=\"pull-right pr-2\" [routerLink]=\"'/'\">\r\n            <i class='fa fa-home'>\r\n\r\n            </i>\r\n\r\n        </div>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/components/nav-bar/nav-bar.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/nav-bar/nav-bar.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/nav-bar/nav-bar.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/nav-bar/nav-bar.component.ts ***!
  \*********************************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavBarComponent = /** @class */ (function () {
    function NavBarComponent() {
        this.type = "base";
        this.type_base = false;
        this.type_organisation = false;
        this.loggedIn = false;
    }
    NavBarComponent.prototype.ngOnInit = function () {
        if (this.type != null && this["type_" + this.type] != null) {
            this["type_" + this.type] = true;
        }
    };
    NavBarComponent.prototype.changeLogout = function (state) {
        this.loggedIn = state;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], NavBarComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], NavBarComponent.prototype, "loading", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], NavBarComponent.prototype, "type", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.nav-bar-base'),
        __metadata("design:type", Boolean)
    ], NavBarComponent.prototype, "type_base", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.nav-bar'),
        __metadata("design:type", Boolean)
    ], NavBarComponent.prototype, "type_organisation", void 0);
    NavBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav-bar',
            template: __webpack_require__(/*! ./nav-bar.component.html */ "./src/app/components/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__(/*! ./nav-bar.component.scss */ "./src/app/components/nav-bar/nav-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "./src/app/components/side-bar/side-bar.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/side-bar/side-bar.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class='sidebar-title'>\r\n    <i class='fa fa-address-card mr-2'></i>Markdown\r\n</div>\r\n<div class='sidebar-links'>\r\n    <div *ngFor=\"let group of navGroups\">\r\n        <div class='header'>\r\n            <i [class]=\"'fa header-icon ' + group.icon\"></i> {{group.title}}\r\n        </div>\r\n        <a class='link' [routerLink]=\"child.route\" (click)=\"navigate($event, child.route)\" *ngFor=\"let child of children[group.title]\">\r\n            {{child.title}}\r\n        </a>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/side-bar/side-bar.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/side-bar/side-bar.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidebar-links {\n  padding-left: 20px; }\n\n.sidebar-links .header, .sidebar-links .link {\n  font-size: small;\n  padding-top: 8px;\n  padding-bottom: 8px;\n  color: #f2f2f2; }\n\n.sidebar-links .header-icon {\n  width: 20px; }\n\n.sidebar-links .header {\n  border-top: rgba(0, 0, 0, 0.1);\n  border-top-style: solid;\n  border-top-width: 1px;\n  border-bottom: rgba(0, 0, 0, 0.1);\n  border-bottom-style: solid;\n  border-bottom-width: 1px;\n  padding-left: 20px; }\n\n.sidebar-links .link {\n  display: block;\n  cursor: pointer;\n  color: #e6e5e5;\n  padding-left: 40px;\n  transition: 0.1s; }\n\n.sidebar-links .link:hover {\n  background-color: rgba(0, 0, 0, 0.5);\n  padding-left: 40px;\n  transition: 0.5s ease-out; }\n"

/***/ }),

/***/ "./src/app/components/side-bar/side-bar.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/side-bar/side-bar.component.ts ***!
  \***********************************************************/
/*! exports provided: SideBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideBarComponent", function() { return SideBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SideBarComponent = /** @class */ (function () {
    function SideBarComponent(router) {
        this.router = router;
        this.onNavigate = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.children = {};
    }
    SideBarComponent.prototype.updateChildren = function () {
        for (var key in this.navGroups) {
            var navGroup = this.navGroups[key];
            if (navGroup.title != null) {
                if (this.children[navGroup.title] == null) {
                    this.children[navGroup.title] = [];
                }
                if (navGroup.children != null) {
                    for (var childKey in navGroup.children) {
                        var route = navGroup.children[childKey];
                        var item = {
                            title: childKey,
                            route: route
                        };
                        this.children[navGroup.title].push(item);
                    }
                }
            }
        }
    };
    SideBarComponent.prototype.ngOnInit = function () {
        this.updateChildren();
    };
    SideBarComponent.prototype.ngOnChanges = function (changes) {
        this.updateChildren();
    };
    SideBarComponent.prototype.navigate = function (event, url) {
        event.stopPropagation();
        this.onNavigate.emit(url);
        this.router.navigate([url]);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], SideBarComponent.prototype, "navGroups", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], SideBarComponent.prototype, "onNavigate", void 0);
    SideBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-side-bar',
            template: __webpack_require__(/*! ./side-bar.component.html */ "./src/app/components/side-bar/side-bar.component.html"),
            styles: [__webpack_require__(/*! ./side-bar.component.scss */ "./src/app/components/side-bar/side-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], SideBarComponent);
    return SideBarComponent;
}());



/***/ }),

/***/ "./src/app/components/welcome/welcome.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [class.nav-sidebar-shown]=\"navExpanded\" class='h-screen'>\r\n    <div class='nav-sidebar-toggle' (click)=\"toggleNav()\">\r\n        <i class='fa fa-bars'></i>\r\n    </div>\r\n    <div class='nav-fader' (click)=\"toggleNav()\">\r\n\r\n    </div>\r\n    <!-- Sidebar -->\r\n    <app-side-bar [navGroups]=\"navGroups\" (onNavigate)=\"onNavigate($event)\"></app-side-bar>\r\n    <!-- /#sidebar-wrapper -->\r\n    <app-nav-bar [type]=\"'organisation'\" [title]=\"'Markdown'\" class='z-depth-1'></app-nav-bar>\r\n    <div class=\"page-content-wrapper\">\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/welcome/welcome.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/welcome/welcome.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.ts ***!
  \*********************************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_discount_records_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/discount_records.service */ "./src/app/services/discount_records.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent(_discountRecordsOService, router, route) {
        this._discountRecordsOService = _discountRecordsOService;
        this.router = router;
        this.route = route;
        this.navExpanded = false;
        this.categories = null;
        this.loaded = false;
        this.navGroups = [];
    }
    WelcomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.navGroups = [];
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            var state = _this._discountRecordsOService.getCategories(lat, lng).subscribe(function (data) {
                if (data != null && data.data != null) {
                    _this.categories = data.data;
                    var categoriesGroup = {
                        title: 'Categories',
                        icon: 'fa-users',
                        children: {}
                    };
                    _this.navGroups = [{
                            title: 'Our Recommendations',
                            icon: 'fa-users',
                            children: {
                                'Cafes': 'category/cafes',
                                'Accommodations': 'category/accommodation',
                                'Clothing': 'category/clothing',
                                'Travel': 'category/Travel%20Agencies'
                            }
                        }];
                    for (var categoryKey in _this.categories) {
                        var cat = _this.categories[categoryKey];
                        categoriesGroup.children[cat] = 'category/' + cat;
                    }
                    _this.navGroups.push(categoriesGroup);
                    _this.loaded = true;
                }
                else {
                    _this.router.navigate(['error/organisation-view/unable to load organisation']);
                }
            }, function (error) {
                console.log(error);
            });
            _this.navGroups = [{
                    title: 'Loading',
                    icon: 'fa-loading',
                }];
        });
    };
    WelcomeComponent.prototype.toggleNav = function () {
        this.navExpanded = !this.navExpanded;
    };
    WelcomeComponent.prototype.setNav = function (status) {
        this.navExpanded = status;
    };
    WelcomeComponent.prototype.onNavigate = function (route) {
        if (window.outerWidth < 768) {
            this.setNav(false);
        }
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/components/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.scss */ "./src/app/components/welcome/welcome.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_discount_records_service__WEBPACK_IMPORTED_MODULE_2__["DiscountRecordsService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/app/config/app.ts":
/*!*******************************!*\
  !*** ./src/app/config/app.ts ***!
  \*******************************/
/*! exports provided: config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "config", function() { return config; });
var config = {
    apps: {
        api: 'https://govhack2018rok.appspot.com/api/'
    }
};


/***/ }),

/***/ "./src/app/services/configuration.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/configuration.service.ts ***!
  \***************************************************/
/*! exports provided: ConfigurationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationService", function() { return ConfigurationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../config/app */ "./src/app/config/app.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// Interfaces here
var ConfigurationService = /** @class */ (function () {
    function ConfigurationService() {
    }
    ConfigurationService.prototype.getConfig = function (dir, def) {
        if (def === void 0) { def = null; }
        var dirSplit = dir.split('.');
        var current = _config_app__WEBPACK_IMPORTED_MODULE_1__["config"];
        for (var i in dirSplit) {
            var val = dirSplit[i];
            if (current[val] == null) {
                return null;
            }
            else {
                current = current[val];
            }
        }
        return current.toString();
    };
    ConfigurationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ConfigurationService);
    return ConfigurationService;
}());



/***/ }),

/***/ "./src/app/services/discount_records.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/discount_records.service.ts ***!
  \******************************************************/
/*! exports provided: DiscountRecordsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiscountRecordsService", function() { return DiscountRecordsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _configuration_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./configuration.service */ "./src/app/services/configuration.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DiscountRecordsService = /** @class */ (function () {
    function DiscountRecordsService(http, Config) {
        this.http = http;
        this.Config = Config;
    }
    DiscountRecordsService.prototype.getDiscountRecord = function (recordId) {
        var url = this.Config.getConfig('apps.api');
        url = url + 'v1/discount-record/' + recordId;
        if (url != null) {
            return this.http.get(url);
        }
        else {
            return null;
        }
    };
    DiscountRecordsService.prototype.getDiscountRecords = function (lat, lng, conditions) {
        var url = this.Config.getConfig('apps.api');
        url = url + 'v1/discount-records/ ' + lat + ' / ' + lng;
        if (conditions != null) {
            var conditionStrings = [];
            for (var key in conditions) {
                conditionStrings.push(key + '=' + conditions[key]);
            }
            if (conditionStrings.length > 0) {
                url = url + '?' + conditionStrings.join("&");
            }
        }
        console.log(encodeURI(url));
        if (url != null) {
            return this.http.get(url);
        }
        else {
            return null;
        }
    };
    DiscountRecordsService.prototype.getCategories = function (lat, lng) {
        if (lat === void 0) { lat = null; }
        if (lng === void 0) { lng = null; }
        var url = this.Config.getConfig('apps.api');
        url = url + 'v1/categories/';
        if (lat != null && lng != null) {
            url = url + lat + "/" + lng;
        }
        if (url != null) {
            return this.http.get(url);
        }
        else {
            return null;
        }
    };
    DiscountRecordsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _configuration_service__WEBPACK_IMPORTED_MODULE_2__["ConfigurationService"]])
    ], DiscountRecordsService);
    return DiscountRecordsService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\dev\apps\govhack\markdown\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map