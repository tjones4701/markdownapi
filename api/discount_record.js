const util = require('../helpers/util');

let apis = [];
apis.push({
    path: "discount-record/:id",
    version: 1,
    public: true,
    get: (app, req, res) => {
        return new Promise((resolve, reject) => {
            let id = req.getParam("id");
            app.db.discount_record.findById(id).then((discountRecord) => {
                if (discountRecord != null) {
                    console.log(discountRecord);
                    resolve(discountRecord);
                } else {
                    reject(404);
                }
            }).catch((error) => {
                console.log(error);
                reject(404)
            });
        });
    }
});

apis.push({
    path: "discount-records/:lat/:lng",
    version: 1,
    public: true,
    get: (app, req, res) => {
        return new Promise((resolve, reject) => {
            let lat = req.getParam("lat");
            let lng = req.getParam("lng");
            let limit = req.getParam("limit");
            let category = req.getParam("category");
            let maxdistance = req.getParam("max-distance");

            app.db.discount_record.findAll().then((discountRecords) => {
                if (discountRecords != null) {
                    var returnRecords = [];
                    for (discountKey in discountRecords) {
                        let discountRecord = discountRecords[discountKey];
                        var discountCategory = discountRecord.dataValues.category.toLowerCase().trim();
                        if (category != null) {
                            category = category.toLowerCase().trim();
                        }

                        if (category == null || category == discountCategory) {
                            if (discountRecord.lat != null && discountRecord.lng != null) {
                                let distance = util.calculateDistance(lat, lng, discountRecord.lat, discountRecord.lng);
                                if (maxdistance == null || distance <= maxdistance) {

                                    discountRecord.dataValues.distance = distance;
                                    returnRecords.push(discountRecord);
                                }
                            }
                        }
                    }

                    returnRecords.sort(function (a, b) { return -(a.dataValues.distance - b.dataValues.distance) });
                    returnRecords.reverse();
                    if (limit != null) {
                        returnRecords = returnRecords.splice(0, limit);
                    }

                    resolve(returnRecords);
                } else {
                    reject(404);
                }
            }).catch((error) => {
                console.log(error);
                reject(404)
            });
        });
    }
});

exports.apis = apis;
