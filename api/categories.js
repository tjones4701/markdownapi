const util = require('../helpers/util');

let apis = [];

apis.push({
    path: "categories",
    version: 1,
    public: true,
    get: (app, req, res) => {
        return new Promise((resolve, reject) => {
            let lat = req.getParam("lat");
            let lng = req.getParam("lng");
            let limit = req.getParam("limit");
            app.db.discount_record.findAll().then((discountRecords) => {
                if (discountRecords != null) {
                    var returnRecords = [];
                    for (discountKey in discountRecords) {
                        let discountRecord = discountRecords[discountKey].dataValues;
                        if (discountRecord.lat != null && discountRecord.lng != null) {
                            let distance = null;

                            if (lat != null && lng != null) {
                                let distance = util.calculateDistance(lat, lng, discountRecord.lat, discountRecord.lng);
                            }

                            if (distance == null || distance <= maxdistance) {
                                if (returnRecords[discountRecord.category] == null) {
                                    returnRecords[discountRecord.category] = true;
                                }
                            }

                        }
                        var returnRecords = Object.keys(returnRecords);
                        returnRecords.sort();

                        resolve(returnRecords);
                    }
                } else {
                    reject(404);
                }
            }).catch((error) => {
                console.log(error);
                reject(404)
            });
        });
    }
});

apis.push({
    path: "categories/:lat/:lng",
    version: 1,
    public: true,
    get: (app, req, res) => {
        return new Promise((resolve, reject) => {
            let lat = req.getParam("lat");
            let lng = req.getParam("lng");
            let maxdistance = req.getParam("max-distance", 50);
            app.db.discount_record.findAll().then((discountRecords) => {
                if (discountRecords != null) {
                    var returnRecords = [];

                    for (discountKey in discountRecords) {

                        let discountRecord = discountRecords[discountKey].dataValues;

                        if (discountRecord.lat != null && discountRecord.lng != null) {
                            let distance = null;

                            if (lat != null && lng != null) {
                                distance = util.calculateDistance(lat, lng, discountRecord.lat, discountRecord.lng);
                            }

                            if (distance == null || distance <= maxdistance) {
                                if (returnRecords[discountRecord.category] == null) {
                                    returnRecords[discountRecord.category] = true;
                                }
                            }

                        }
                    }
                    var returnRecords = Object.keys(returnRecords);
                    returnRecords.sort();
                    resolve(returnRecords);
                } else {
                    reject(404);
                }
            }).catch((error) => {
                console.log(error);
                reject(404)
            });
        });
    }
});

exports.apis = apis;
