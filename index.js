
// call the packages we need
const express = require('express');        // call express
const app = express();                 // define our app using express

const routeBuilder = require('./routeBuilder');
const database = require('./database');
const configLoader = require('./helpers/configLoader');


if (process.env.GCLOUD_PROJECT) {
    require('@google-cloud/debug-agent').start();
}

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
// in latest body-parser use like below.
app.use(bodyParser.urlencoded({ extended: true }));



var config = configLoader(__dirname);



const cors = require('cors')


app.use(cors())


//Helpers
const queryHelper = require('./helpers/query');
app.helpers = {};
app.helpers.query = queryHelper;

app.sequelize = database;
app.db = app.sequelize;


var storeOptions = {};

app.use(express.static("public"));


var apis = {};

require('fs').readdirSync(__dirname + '/api/').forEach(function (file) {
    if (file.match(/\.js$/) !== null && file !== 'index.js') {

        var name = file.replace('.js', '');

        var apiExport = require('./api/' + file);

        if (apiExport.apis != null) {
            var apiList = apiExport.apis;

            for (var i in apiList) {
                var api = apiList[i];

                if (api.path != null && api.version != null) {
                    apis['/v' + api.version + '/' + api.path] = api;
                }
            }
        }
    }
});


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router



router = routeBuilder(app, apis, router);

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}, router);


app.use(express.static(__dirname + '/app'));

app.use(cors());
app.options('*', cors());

// START THE SERVER
// =============================================================================
app.listen(config.port, function (e) {

});
console.log('Magic happens on port ' + config.port);
