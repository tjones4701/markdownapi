console.log("---------------------------");
console.log("Initiating Database");
console.log("---------------------------");


const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + "/config/database.json")[env];
const modelUri = __dirname + "/models";
const db = {};



let sequelize;

if (env == "production") {

    sequelize = new Sequelize(
        config.database, process.env.DB_USERNAME, process.env.DB_PASSWORD, config
    );
} else {
    sequelize = new Sequelize(
        config.database, config.username, config.password, config
    );
}

var models = [];

fs
    .readdirSync(modelUri)
    .filter(file =>
        (file.indexOf('.') !== 0) &&
        (file !== basename) &&
        (file.slice(-3) === '.js'))
    .forEach(file => {
        const model = sequelize.import(path.join(modelUri, file));
        models.push(model.name);
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

console.log(models.join(", "));

module.exports = db;
